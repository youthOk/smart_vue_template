import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './permission.js'

import '@/styles/index.scss'
// element.ui
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

Vue.config.productionTip = false

// svg
import '@/icons'

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
}).$mount('#app')
