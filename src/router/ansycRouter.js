export default [
  {
    'path': '/home',
    'icon': 'speedometer',
    'name': '首页',
    'redirect': '/home/main/mallboard',
    'meta': { title: '首页', icon: 'home', breadcrumb: false },
    'children': [
      {
        'path': 'main',
        'name': '首页',
        'component': 'template',
        'redirect': '/home/main/mallboard',
        'meta': { title: '首页', icon: 'home', breadcrumb: true },
        children: [
          {
            'path': 'mallboard',
            'name': '商城数据看板',
            'component': 'template',
            'redirect': '/home/main/mallboard/mallboard1',
            meta: { title: '商城数据看板', icon: 'costoms-alearance' , role:'111'},
            children: [
              {
                'path': 'mallboard1',
                'name': '商城数据看板-21',
                'component': 'home/mallboard',
                meta: { title: '商城数据看板21'}
              },
              {
                'path': 'mallboard2',
                'name': '商城数据看板-22',
                'component': 'home/mallboard2',
                meta: { title: '商城数据看板22'}
              },
              {
                'path': 'mallboard3',
                'name': '商城数据看板-23',
                'component': 'home/mallboard3',
                meta: { title: '商城数据看板23'}
              },
            ]
          },
          {
            'path': 'payboard',
            'name': '支付数据看板',
            'component': 'home/payboard',
            meta: { title: '支付数据看板', icon: 'trust-fill' }
          },
          {
            'path': 'orderboard',
            'name': '订单数据看板',
            'component': 'home/orderboard',
            meta: { title: '订单数据看板', icon: 'electronics' }
          }
        ]
      }

    ]
  },
  {
    'path': '/organization',
    'icon': 'speedometer',
    'name': '组织',
    'redirect': '/organization/main/department',
    'meta': { title: '组织', icon: 'feeds', breadcrumb: false },
    'children': [
      {
        'path': 'main',
        'name': '组织机构',
        'component': 'template',
        'redirect': '/organization/main/department',
        'meta': { title: '组织机构', icon: 'feeds', breadcrumb: true },
        children: [
          {
            'path': 'department',
            'name': '部门管理',
            'component': 'organization/department',
            'meta': { title: '部门管理', icon: 'form' }
          },
          {
            'path': 'role',
            'name': '角色权限',
            'component': 'organization/role',
            meta: { title: '角色权限', icon: 'jewelry' }
          },
          {
            'path': 'staff',
            'name': '员工管理',
            'component': 'organization/staff',
            meta: { title: '员工管理', icon: 'similar-product-fill' }
          }
        ]
      },
      {
        'path': 'module',
        'name': '模块管理',
        component: 'system/module',
        'meta': { title: '模块管理', icon: 'packaging' }
      }
    ]
  },
  {
    'path': '/client',
    'icon': 'speedometer',
    'name': '设置',
    'redirect': '/client/module',
    'meta': { title: '日志', icon: 'xiakuangxian', breadcrumb: false },
    'children': [
      {
        'path': 'module',
        'name': '菜单设置',
        component: 'system/module',
        'meta': { title: '菜单设置', icon: 'xiakuangxian' }
      },
      {
        'path': 'actical',
        'name': '文章设置',
        component: 'system/module',
        'meta': { title: '文章设置', icon: 'folder-fill' }
      }
    ]
  },
  {
    'path': '/system',
    'icon': 'speedometer',
    'name': '系统',
    'redirect': '/system/module/adminmanger',
    'meta': { title: '系统', icon: 'Moneymanagement-fill', breadcrumb: false },
    'children': [
      {
        'path': 'module',
        'name': '模块管理',
        component: 'template',
        'redirect': '/system/module/adminmanger',
        'meta': { title: '模块管理', icon: 'store', breadcrumb: true },
        'children': [
          {
            'path': 'adminmanger',
            'name': '后台管理',
            'component': 'system/adminmanger',
            meta: { title: '后台管理', icon: 'tool-hardware' }
          }
        ]

      }, {
        'path': 'wordbook',
        'icon': 'speedometer',
        component: 'template',
        'redirect': '/system/wordbook/wordbookmanager',
        'name': '字典',
        'meta': { title: '字典', icon: 'leftalignment', breadcrumb: true },
        'children': [
          {
            'path': 'wordbookmanager',
            'name': '字典管理',
            'component': 'system/wordbookmanager',
            meta: { title: '字典管理', icon: 'all-fill1' }
          }
        ]
      },
      {
        'path': 'log',
        'icon': 'speedometer',
        'name': '列表管理',
        component: 'template',
        'redirect': '/system/log/actionlog',
        'meta': { title: '列表管理', icon: 'warehouse', breadcrumb: true },
        'children': [
          {
            'path': 'actionlog',
            'name': '操作列表',
            'component': 'system/actionlog',
            meta: { title: '操作列表', icon: 'contacts-fill' }
          },
          {
            'path': 'systemlog',
            'name': '系统列表',
            'component': 'system/systemlog',
            meta: { title: '系统列表', icon: 'invoice' }
          }
        ]
      }


    ]
  }

]
